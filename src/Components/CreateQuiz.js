import React, { Component } from 'react';
import '../App.css';
import fire from '../fire.js';

class CreateQuiz extends Component{
  constructor(props) {
    super(props);
    this.state = {
      assignmentID: '',
      assignmentDetailsID:'', 
      assignmentTitle: '',
      assignmentInstruction: '',
      revealScore: false,
      displayOneQuestion: false,
      shuffleQuestion: false,
      addTimer: false
    };
  }

  handleInput=e=>{
    this.setState({
      [e.target.id] : e.target.value
    })
    // console.log(this.state);
  }

  handleSubmit= e =>{
  let assignmentDetailsParameters = {
    assignmentInstruction: this.state.instructions,
    revealScore: this.state.revealScore,
    displayOneQuestion: this.state.displayOneQuestion,
    shuffleQuestion: this.state.shuffleQuestion,
    addTimer: this.state.addTimer
  }
    
    const assignmentDetailsRef = fire.database().ref('assignmentDetails').push(assignmentDetailsParameters);
    const assignmentDetailsUID = assignmentDetailsRef.key;

    let assignmentParameters = {
      assignmentTitle: this.state.title,
      assignmentDetailsID: assignmentDetailsUID
    }

    const assignmentRef = fire.database().ref('assignment').push(assignmentParameters);
    const assignmentUID = assignmentRef.key;

    this.setState({
      assignmentID: assignmentUID,
      assignmentDetailsID:assignmentDetailsUID, 
      assignmentTitle: '',
      assignmentInstruction: '',
      revealScore: false,
      displayOneQuestion: false,
      shuffleQuestion: false,
      addTimer: false
    });
  }

  render(){
    return (
      <div className = 'App'>

        {/* <form action=""> */}
          <div>
            <label htmlFor="title" className= 'm-2'>Title:</label><br />
            <input onChange={this.handleInput} id="title" type="text" name="title" required className= 'm-2'/><br />
          </div>
          <div>
            <label htmlFor="instructions" className= 'm-2'>Instructions:</label><br />
            <textarea onChange={this.handleInput} rows="3" id="instructions" className= "m-2"></textarea>        
          </div>
            <input onChange={this.handleInput} type="checkbox" id="revealScore" name="revealScore" value="true" />
            <label htmlFor="revealScore"> Reveal Sore</label><br />

            <input onChange={this.handleInput} type="checkbox" id="displayOneQuestion" name="displayOneQuestion" value="true" />
            <label htmlFor="displayOneQuestion"> Display one question per page</label><br />

            <input onChange={this.handleInput} type="checkbox" id="shuffleQuestion" name="shuffleQuestion" value="true" />
            <label htmlFor="shuffleQuestion"> Shuffle question order</label><br />

            <input onChange={this.handleInput} type="checkbox" id="addTimer" name="addTimer" value="true" />
            <label htmlFor="addTimer">Add timer</label><br />

            <button type="button" onClick={this.handleSubmit} className="btn btn-secondary btn-sm m-2">Create</button>
            <button type="button" className="btn btn-secondary btn-sm m-2">Cancel</button>
        {/* </form>          */}

      </div>
    );
  }
}

export default CreateQuiz;
